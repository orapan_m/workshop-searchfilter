import logo from "./logo.svg";
// import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import { useEffect, useState } from "react";

function App() {
  const [countries, setCountries] = useState([]);
  const [word, setWord] = useState("Thailand");
  const [dataFilter, setDataFilter] = useState(["name"]);

  useEffect(() => {
    const url = "https://restcountries.com/v2/all";
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        setCountries(data);
      });
  });

  const searchCountries = (countries) => {
    return countries.filter((item) => {
      return dataFilter.some((filter) => {
        return (
          typeof item[filter] !== "undefined" &&
          item[filter]
            .toString()
            .toLowerCase()
            .indexOf(word.toString().toLowerCase()) > -1
        );
      });
    });
  };

  const formatNumber = (num) => {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  };

  return (
    <Container fluid="md">
      <Row>
        <Col>
          <h1>Search Filter</h1>
        </Col>
      </Row>
      <Row>
        <Col>
          <InputGroup className="mb-3">
            <Form.Control
              placeholder="Search"
              aria-label="Search"
              aria-describedby="basic-addon2"
              onChange={(e) => setWord(e.target.value)}
              value={word}
            />
          </InputGroup>
        </Col>
      </Row>
      <Row className="justify-content-md-center">
        {searchCountries(countries).map((item, index) => (
          <Col key={index} sm={4} md="auto">
            <Card style={{ width: "18rem" }}>
              <Card.Img
                variant="top"
                src={item.flags["png"]}
                alt={item.name}
                width={200}
                height={200}
              />
              <Card.Body>
                <Card.Title>{item.name}</Card.Title>
                <ListGroup>
                  <ListGroup.Item>
                    People : {formatNumber(item.population)}
                  </ListGroup.Item>
                  <ListGroup.Item>Region : {item.region}</ListGroup.Item>
                  <ListGroup.Item>Capital : {item.capital}</ListGroup.Item>
                </ListGroup>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
}

export default App;
